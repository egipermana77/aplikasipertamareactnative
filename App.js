import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  TextInput,
  StyleSheet,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

// import SampleComponent from './SampleComponent';
// import StylingComponent from './StylingComponent';
// import MateriFlexbox from './Components/MateriFlexbox.js';
// import PositionReact from './Components/PositionReact.js';
// import PropsDinamis from './Components/PropsDinamis';
// import StateDinamis from './Components/stateDinamis/';
// import CommunicationComponent from './Components/CommunicationComponent/';
// import ReactNativeSVG from './Components/ReactNativeSVG/';
// import CallAPIVanilla from './Components/CallAPIVanilla/';
// import CallAPIAxios from './Components/CallAPIAxios/';
// import CallAPILocal from './Components/CallAPILocal/';
// import NotifComponent from './Components/NotifComponent/';
import Router from './src/router';
import {Provider} from 'react-redux';
import {store} from './redux';

const App = () => {
  // const [isShow, setIsShow] = useState(true);
  //untuk pengganti didmount bisa gunakan useeffect
  // useEffect(() => {
  //   setTimeout(() => {
  //     setIsShow(false);
  //   }, 6000);
  // });

  return (
    <Provider store={store}>
      <NavigationContainer>
        {/*<View>
        <ScrollView>
        {isShow && <MateriFlexbox />}
        <SampleComponent />
        <PropsDinamis />
        <StateDinamis />
        <StylingComponent />
        <PositionReact />
        <CommunicationComponent />
        <ReactNativeSVG />
        <CallAPIVanilla />
        <CallAPIAxios />
        <CallAPILocal />
      </ScrollView>
      <NotifComponent />
      </View>*/}
        <Router />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
