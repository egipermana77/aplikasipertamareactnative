import React, {useState, useEffect} from 'react';
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	Button,
	Image,
	TouchableOpacity,
	Alert,
} from 'react-native';
import Axios from 'axios';

const Item = ({name, email, urutan, onPress, onDelete}) => {
	return (
		<View style={styles.itemContainer}>
			<TouchableOpacity onPress={onPress}>
				<Image
					source={{uri: `https://i.pravatar.cc/150?u=${email}`}}
					style={styles.avatar}
				/>
			</TouchableOpacity>
			<View style={styles.desc}>
				<Text style={styles.descName}>{name}</Text>
				<Text style={styles.descEmail}>{email}</Text>
				<Text style={styles.descUrutan}>Kenpachi Ke : {urutan} </Text>
			</View>
			<TouchableOpacity onPress={onDelete}>
				<Text style={styles.delete}>X</Text>
			</TouchableOpacity>
		</View>
	);
};

const LocalAPI = () => {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [urutan, setUrutan] = useState('');
	const [users, setUsers] = useState([]);
	const [button, setButton] = useState('Save');
	const [selectedUser, setSelectedUser] = useState({});

	useEffect(() => {
		getData();
	}, []);

	const submit = () => {
		const data = {name, email, urutan};
		if (button === ' Save') {
			Axios.post('http://10.0.3.2:3004/users', data).then(res => {
				// console.log('res', res);
				setName('');
				setEmail('');
				setUrutan('');
				getData();
			});
		} else if (button === 'Update') {
			Axios.put(`http://10.0.3.2:3004/users/${selectedUser.id}`, data).then(
				res => {
					// console.log('res update :', res);
					setName('');
					setEmail('');
					setUrutan('');
					getData();
					setButton('Save');
				},
			);
		}
	};

	const getData = () => {
		Axios.get('http://10.0.3.2:3004/users').then(res => {
			// console.log('res ', res);
			setUsers(res.data);
		});
	};

	const selectItem = item => {
		// console.log(item);
		setSelectedUser(item);
		setName(item.name);
		setEmail(item.email);
		setUrutan(item.urutan);
		setButton('Update');
	};

	const deleteItem = item => {
		// console.log('delete item', item);
		Axios.delete(`http://10.0.3.2:3004/users/${item.id}`).then(res => {
			console.log('delete', res);
			getData();
		});
	};

	return (
		<View style={styles.container}>
			<Text style={styles.textTitle}>Local API (JSON Server)</Text>
			<Text>Masukan Anggota Kenpachi</Text>
			<TextInput
				placeholder="Nama Lengkap ya"
				style={styles.input}
				value={name}
				onChangeText={value => setName(value)}
			/>
			<TextInput
				placeholder="Email"
				style={styles.input}
				value={email}
				onChangeText={value => setEmail(value)}
			/>
			<TextInput
				placeholder="Kenpachi Ke"
				style={styles.input}
				value={urutan.toString()}
				onChangeText={value => setUrutan(value)}
			/>
			<Button title={button} onPress={submit} />
			<View style={styles.line} />
			{users.map(user => {
				return (
					<Item
						key={user.id}
						name={user.name}
						email={user.email}
						urutan={user.urutan}
						onPress={() => selectItem(user)}
						onDelete={() =>
							Alert.alert('Peringatan', 'Anda yakin hapus data ?', [
								{
									text: 'Tidak',
									onPress: () => console.log('button tidak'),
								},
								{
									text: 'Ya',
									onPress: () => deleteItem(user),
								},
							])
						}
					/>
				);
			})}
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		padding: 20,
	},
	textTitle: {
		textAlign: 'center',
		marginBottom: 20,
	},
	line: {
		height: 2,
		backgroundColor: 'black',
		marginVertical: 20,
	},
	input: {
		borderWidth: 1,
		marginBottom: 12,
		borderRadius: 25,
		paddingHorizontal: 18,
	},
	avatar: {
		width: 80,
		height: 80,
		borderRadius: 80,
	},
	itemContainer: {
		flexDirection: 'row',
		marginBottom: 20,
	},
	desc: {
		marginLeft: 18,
		flex: 1,
	},
	descName: {fontSize: 20, fontWeight: 'bold'},
	descEmail: {fontSize: 16},
	descUrutan: {fontSize: 12, marginTop: 8},
	delete: {fontSize: 20, fontWeight: 'bold', color: 'red'},
});

export default LocalAPI;
