import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Button, Image} from 'react-native';

const CallAPIVanilla = () => {
	const [dataUser, setDataUser] = useState({
		avatar: '',
		email: '',
		first_name: '',
		last_name: '',
	});
	const [dataPost, setDataPost] = useState({
		name: '',
		job: '',
	});
	useEffect(() => {
		//Call API Method GET
		// fetch('https://reqres.in/api/users/2')
		// 	.then(response => response.json())
		// 	.then(json => console.log(json));
		//Call API Method POST
		// const dataForAPI = {
		// 	name: 'prawito hudoro',
		// 	job: 'mentor',
		// };
		// console.log('data berupa object :', dataForAPI);
		// console.log('data berupa json stringify : ', JSON.stringify(dataForAPI));
		// fetch('https://reqres.in/api/users', {
		// 	method: 'POST',
		// 	headers: {
		// 		'Content-Type': 'application/json',
		// 	},
		// 	body: JSON.stringify(dataForAPI),
		// })
		// 	.then(response => response.json())
		// 	.then(json => {
		// 		console.log('response json', json);
		// 	});
	}, []);

	const getData = () => {
		fetch('https://reqres.in/api/users/2')
			.then(response => response.json())
			.then(json => {
				console.log(json);
				setDataUser(json.data);
			});
	};

	const postData = () => {
		const dataForAPI = {
			name: 'prawito hudoro',
			job: 'mentor',
		};
		fetch('https://reqres.in/api/users', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(dataForAPI),
		})
			.then(response => response.json())
			.then(json => {
				console.log('response json', json);
				setDataPost(json);
			});
	};

	return (
		<View style={styles.container}>
			<Text style={styles.textTitle}>Call API Dengan Vanilla JS</Text>
			<Button title="GET DATA" onPress={getData} />
			<Image source={{uri: dataUser.avatar}} style={styles.avatar} />
			<Text>Response GET DATA</Text>
			<Text>{`${dataUser.first_name} ${dataUser.last_name}`}</Text>
			<Text>{dataUser.email}</Text>
			<View style={styles.line} />
			<Button title="POST DATA" onPress={postData} />
			<Text>Response POST DATA</Text>
			<Text>{dataPost.name}</Text>
			<Text>{dataPost.job}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		padding: 20,
	},
	textTitle: {
		textAlign: 'center',
	},
	line: {
		height: 2,
		backgroundColor: 'black',
		marginVertical: 20,
	},
	avatar: {
		width: 100,
		height: 100,
		borderRadius: 100,
	},
});

export default CallAPIVanilla;
