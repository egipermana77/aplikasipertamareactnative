import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Cart from './../CartComponent/';
import Product from './../ProductComponent/';

const CommunicationComponent = () => {
	const [totalProduct, setTotalProduct] = useState(0);
	return (
		<View style={styles.wrapper}>
			<Text style={styles.textTitle}>Komunikasi Antar Component</Text>
			<Cart quantity={totalProduct} />
			<Product onButtonPress={() => setTotalProduct(totalProduct + 1)} />
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		padding: 20,
	},
	textTitle: {
		textAlign: 'center',
	},
});

export default CommunicationComponent;
