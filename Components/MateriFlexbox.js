import React, {Component, useState, useEffect} from 'react';
import {Text, View, Image} from 'react-native';

// class MateriFlexbox extends Component {
// 	constructor(props) {
// 		super(props);
// 		console.log('constructor');
// 		this.state = {
// 			subscriber: 1000,
// 		};
// 	}

// 	componentDidMount() {
// 		console.log('componentDidMount');
// 		setTimeout(() => {
// 			this.setState({
// 				subscriber: 400,
// 			});
// 		}, 2000);
// 	}

// 	componentDidUpdate() {
// 		console.log('componentDidUpdate');
// 	}

// 	componentWillUnmount() {
// 		console.log('componentWillUnmount');
// 	}

// 	render() {
// 		console.log('render');
// 		return (
// 			<View>
// 				<View
// 					style={{
// 						flexDirection: 'row',
// 						backgroundColor: '#bdc3c7',
// 						alignItems: 'center',
// 						justifyContent: 'space-between',
// 					}}
// 				>
// 					<View
// 						style={{backgroundColor: '#c0392b', width: 60, height: 50}}
// 					></View>
// 					<View
// 						style={{backgroundColor: '#2980b9', width: 50, height: 50}}
// 					></View>
// 					<View
// 						style={{backgroundColor: '#8e44ad', width: 50, height: 50}}
// 					></View>
// 					<View
// 						style={{backgroundColor: '#f39c12', width: 50, height: 50}}
// 					></View>
// 				</View>
// 				<View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
// 					<Text>Beranda</Text>
// 					<Text>Video</Text>
// 					<Text>Playlist</Text>
// 					<Text>Komunitas</Text>
// 					<Text>Chanel</Text>
// 					<Text>Tentang</Text>
// 				</View>
// 				<View
// 					style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}
// 				>
// 					<Image
// 						source={{uri: 'https://placeimg.com/100/100/people'}}
// 						style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}}
// 					/>
// 					<View>
// 						<Text style={{fontSize: 20, fontWeight: 'bold'}}>Agnes Monica</Text>
// 						<Text>{this.state.subscriber} ribu subscriber</Text>
// 					</View>
// 				</View>
// 			</View>
// 		);
// 	}
// }

const MateriFlexbox = () => {
	const [subscriber, setSubscriber] = useState(200);

	useEffect(() => {
		console.log('did mount');
		//apabila did update disatu useeffect
		setTimeout(() => {
			setSubscriber(400);
		}, 2000);
		return () => {
			console.log('did update');
		};
		//untuk will unmount dimunculkan dg did update untuk use effect, untuk unmount lihat pada file app.js
	}, [subscriber]);
	//didupdate apabila terpisah
	// useEffect(() => {
	// 	console.log('did update');
	// 	setTimeout(() => {
	// 		setSubscriber(400);
	// 	}, 2000);
	// }, [subscriber]);

	return (
		<View>
			<View
				style={{
					flexDirection: 'row',
					backgroundColor: '#bdc3c7',
					alignItems: 'center',
					justifyContent: 'space-between',
				}}
			>
				<View
					style={{backgroundColor: '#c0392b', width: 60, height: 50}}
				></View>
				<View
					style={{backgroundColor: '#2980b9', width: 50, height: 50}}
				></View>
				<View
					style={{backgroundColor: '#8e44ad', width: 50, height: 50}}
				></View>
				<View
					style={{backgroundColor: '#f39c12', width: 50, height: 50}}
				></View>
			</View>
			<View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
				<Text>Beranda</Text>
				<Text>Video</Text>
				<Text>Playlist</Text>
				<Text>Komunitas</Text>
				<Text>Chanel</Text>
				<Text>Tentang</Text>
			</View>
			<View style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
				<Image
					source={{uri: 'https://placeimg.com/100/100/people'}}
					style={{width: 100, height: 100, borderRadius: 50, marginRight: 14}}
				/>
				<View>
					<Text style={{fontSize: 20, fontWeight: 'bold'}}>Agnes Monica</Text>
					<Text>{subscriber} ribu subscriber</Text>
				</View>
			</View>
		</View>
	);
};

export default MateriFlexbox;
