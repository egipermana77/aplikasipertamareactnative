import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import imgCart from './../images/cart.png';

const PositionReact = () => {
	return (
		<View style={styles.wrapper}>
			<View style={styles.cartWrapper}>
				<Image source={imgCart} style={styles.iconCart} />
				<Text style={styles.notif}>10</Text>
			</View>
			<Text style={styles.text}>Keranjang Belanja Anda</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {padding: 20, alignItems: 'center'},
	cartWrapper: {
		width: 93,
		height: 93,
		borderWidth: 1,
		borderColor: '#8e44ad',
		borderRadius: 93 / 2,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'relative',
	},
	iconCart: {width: 50, height: 50},
	text: {fontSize: 12, fontWeight: '700', color: '#f39c12', marginTop: 8},
	notif: {
		fontSize: 12,
		color: 'white',
		backgroundColor: '#2980b9',
		padding: 4,
		borderRadius: 25,
		width: 24,
		height: 24,
		position: 'absolute',
		top: 0,
		right: 0,
	},
});

export default PositionReact;
