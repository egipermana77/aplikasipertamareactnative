import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import wall from './../../images/wall.jpg';

const Product = props => {
	return (
		<View>
			<View style={styles.cardWrapper}>
				<Image source={wall} style={styles.imgCard} />
				<Text style={styles.textTittle}>Tembok Bersejarah 1900</Text>
				<Text style={styles.textPrice}>Rp. 25.000.000</Text>
				<Text style={styles.textCapiton}>Duch, Belanda</Text>
				<TouchableOpacity onPress={props.onButtonPress}>
					<View style={styles.btnWrapper}>
						<Text style={styles.textBeli}>Beli</Text>
					</View>
				</TouchableOpacity>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	cardWrapper: {
		padding: 12,
		backgroundColor: '#F2F2F2',
		width: 212,
		borderRadius: 8,
	},
	imgCard: {
		width: 188,
		height: 107,
		borderRadius: 8,
	},
	textTittle: {
		marginTop: 16,
		fontSize: 14,
		fontWeight: 'bold',
		color: 'black',
	},
	textPrice: {
		fontSize: 12,
		color: 'orange',
		fontWeight: 'bold',
		marginTop: 8,
	},
	textCapiton: {
		marginTop: 6,
		fontSize: 12,
		fontWeight: '300',
	},
	btnWrapper: {
		backgroundColor: 'green',
		marginTop: 12,
		paddingVertical: 6,
		borderRadius: 25,
	},
	textBeli: {
		fontSize: 14,
		fontWeight: '600',
		color: 'white',
		textAlign: 'center',
	},
});

export default Product;
