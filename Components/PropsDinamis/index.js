'use strict';

import React from 'react';

import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';

const Story = props => {
	return (
		<View style={{alignItems: 'center', marginRight: 20}}>
			<Image
				source={{
					uri: props.image,
				}}
				style={{width: 50, height: 50, borderRadius: 25}}
			/>
			<Text style={{maxWidth: 50, textAlign: 'center'}}>{props.title}</Text>
		</View>
	);
};

const PropsDinamis = props => {
	return (
		<View>
			<Text>Materi Component Dengan Props Dinamis</Text>
			<ScrollView horizontal>
				<View style={{flexDirection: 'row'}}>
					<Story
						title="Youtube Chanel 1"
						image="https://instagram.fcgk23-1.fna.fbcdn.net/v/t51.2885-15/s150x150/269765648_101791949019054_6852235042566632955_n.jpg?_nc_ht=instagram.fcgk23-1.fna.fbcdn.net&_nc_cat=106&_nc_ohc=fihhwY9aBcYAX9rhUMK&edm=ALbqBD0BAAAA&ccb=7-4&oh=00_AT87w3d_EANRl3MpjxvTMXPClmvtqbjcYNMXDTOfyVMKAA&oe=61E148FA&_nc_sid=9a90d6"
					/>
					<Story
						title="Youtube Chanel 2"
						image="https://instagram.fcgk23-1.fna.fbcdn.net/v/t51.2885-15/s150x150/269771293_119181613931514_8814320522156859647_n.jpg?_nc_ht=instagram.fcgk23-1.fna.fbcdn.net&_nc_cat=106&_nc_ohc=1Uhf8TQ-PqcAX8hEiyD&edm=ALbqBD0BAAAA&ccb=7-4&oh=00_AT8u8wgmcV6YaiP-Fz8QT7fLsmHWsKCl-Z9sljbepuH6Xg&oe=61DFD2DB&_nc_sid=9a90d6"
					/>
					<Story
						title="Youtube Chanel 3"
						image="https://instagram.fcgk23-1.fna.fbcdn.net/v/t51.2885-15/s150x150/269900125_1307991056334092_732693041734607793_n.jpg?_nc_ht=instagram.fcgk23-1.fna.fbcdn.net&_nc_cat=108&_nc_ohc=N16CXHkYeHAAX9pFL2_&edm=ALbqBD0BAAAA&ccb=7-4&oh=00_AT_mzJwkrJ0wmBhA9rvzrYVu6AP2fCC7y03TcpQ8GwxVTg&oe=61DFD0B3&_nc_sid=9a90d6"
					/>
					<Story
						title="Youtube Chanel 4"
						image="https://instagram.fcgk23-1.fna.fbcdn.net/v/t51.2885-15/s150x150/269765648_101791949019054_6852235042566632955_n.jpg?_nc_ht=instagram.fcgk23-1.fna.fbcdn.net&_nc_cat=106&_nc_ohc=fihhwY9aBcYAX9rhUMK&edm=ALbqBD0BAAAA&ccb=7-4&oh=00_AT87w3d_EANRl3MpjxvTMXPClmvtqbjcYNMXDTOfyVMKAA&oe=61E148FA&_nc_sid=9a90d6"
					/>
					<Story
						title="Youtube Chanel 5"
						image="https://instagram.fcgk23-1.fna.fbcdn.net/v/t51.2885-15/s150x150/269765648_101791949019054_6852235042566632955_n.jpg?_nc_ht=instagram.fcgk23-1.fna.fbcdn.net&_nc_cat=106&_nc_ohc=fihhwY9aBcYAX9rhUMK&edm=ALbqBD0BAAAA&ccb=7-4&oh=00_AT87w3d_EANRl3MpjxvTMXPClmvtqbjcYNMXDTOfyVMKAA&oe=61E148FA&_nc_sid=9a90d6"
					/>
					<Story
						title="Youtube Chanel abah anom"
						image="https://instagram.fcgk23-1.fna.fbcdn.net/v/t51.2885-15/s150x150/269820298_661966668148897_4838527302967889078_n.jpg?_nc_ht=instagram.fcgk23-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=UlwU0szU1MIAX-YccfD&tn=WaXXM94btm6pyUnO&edm=ALbqBD0BAAAA&ccb=7-4&oh=00_AT9XO9R_O1FFo3EsSHqjcGzyp3jqyLwpQATTzo7irkVRTQ&oe=61E11E90&_nc_sid=9a90d6"
					/>
				</View>
			</ScrollView>
		</View>
	);
};

const styles = StyleSheet.create({});

export default PropsDinamis;
