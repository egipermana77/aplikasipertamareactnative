import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import SvgImage from './../../images/teacher.svg';

const ReactNativeSVG = () => {
	return (
		<View style={styles.container}>
			<Text style={styles.textTitle}>Materi Pemanggilan React Native SVG</Text>
			<SvgImage width={200} height={200} />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		padding: 20,
	},
	textTitle: {
		textAlign: 'center',
	},
});

export default ReactNativeSVG;
