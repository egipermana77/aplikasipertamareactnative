import React, {useState, Component} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const Counter = () => {
	const [number, setNumber] = useState(0);
	return (
		<View>
			<Text>{number}</Text>
			<Button title="Tambah" onPress={() => setNumber(number + 1)} />
		</View>
	);
};

// apabila menggunakan class component
class CounterNumber extends Component {
	state = {
		number: 0,
	};

	render() {
		return (
			<View>
				<Text>{this.state.number}</Text>
				<Button
					title="Tambah"
					onPress={() => this.setState({number: this.state.number + 1})}
				/>
			</View>
		);
	}
}

const StateDinamis = () => {
	return (
		<View style={styles.wrapper}>
			<Text style={styles.textTitle}>State Dinamis bgst</Text>
			<Text>Functional Component (Hooks)</Text>
			<Counter />
			<Counter />
			<Text style={styles.fileSection}>Class Component</Text>
			<CounterNumber />
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		padding: 20,
	},
	textTitle: {
		textAlign: 'center',
	},
	fileSection: {
		marginTop: 20,
	},
});

export default StateDinamis;
