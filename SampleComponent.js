import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
} from 'react-native';

const SampleComponent = () => {
  return (
    <View>
      <View style={{width: 80, height: 80, backgroundColor: '#2980b9'}}></View>
      <Text>Agus begadang</Text>
      <Salim />
      <Text>Agustin Salim</Text>
      <Photo />
      <TextInput style={{borderWidth: 1, padding: 10}} />
      <BoxGreen />
      <Profile />
    </View>
  );
};

const Salim = () => {
  return <Text>salim</Text>;
};

const Photo = () => {
  return (
    <Image
      style={{width: 100, height: 100}}
      source={{uri: 'https://placeimg.com/100/100/animals'}}
    />
  );
};

class BoxGreen extends Component {
  render() {
    return <Text>Text dari class Component</Text>;
  }
}

class Profile extends Component {
  render() {
    return (
      <View>
        <Image
          style={{width: 100, height: 100, borderRadius: 50}}
          source={{uri: 'https://placeimg.com/100/100/people'}}
        />
        <Text style={{color: 'blue', fontSize: 24}}>kelly</Text>
      </View>
    );
  }
}

export default SampleComponent;
