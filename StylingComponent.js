import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
} from 'react-native';
import wall from './images/wall.jpg';

const StylingComponent = () => {
  return (
    <View>
      <Text style={styles.text}>Styling Component</Text>
      <View
        style={{
          width: 100,
          height: 100,
          backgroundColor: '#2980b9',
          borderWidth: 2,
          borderColor: 'purple',
          marginTop: 20,
          marginLeft: 20,
        }}
      />
      <View
        style={{
          padding: 12,
          backgroundColor: '#F2F2F2',
          width: 212,
          borderRadius: 8,
        }}
      >
        <Image
          source={wall}
          style={{width: 188, height: 107, borderRadius: 8}}
        />
        <Text
          style={{
            marginTop: 16,
            fontSize: 14,
            fontWeight: 'bold',
            color: 'black',
          }}
        >
          Tembok Bersejarah 1900
        </Text>
        <Text
          style={{
            fontSize: 12,
            color: 'orange',
            fontWeight: 'bold',
            marginTop: 8,
          }}
        >
          Rp. 25.000.000
        </Text>
        <Text style={{marginTop: 6, fontSize: 12, fontWeight: '300'}}>
          Duch, Belanda
        </Text>
        <View
          style={{
            backgroundColor: 'green',
            marginTop: 12,
            paddingVertical: 6,
            borderRadius: 25,
          }}
        >
          <Text
            style={{
              fontSize: 14,
              fontWeight: '600',
              color: 'white',
              textAlign: 'center',
            }}
          >
            Beli
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'green',
    marginLeft: 20,
    marginTop: 20,
  },
});

export default StylingComponent;
