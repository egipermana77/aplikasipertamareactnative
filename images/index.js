import wall from './wall.jpg';
import ArrowIcon from './arrow-left-circle.svg';
import Illustration from './teacher.svg';

export {wall, ArrowIcon, Illustration};
