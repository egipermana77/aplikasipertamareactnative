import {combineReducers} from 'redux';

const initialState = {
	name: 'Apps Pertama saya',
};

/**
 * setting register reducer
 */
const initialStateRegister = {
	form: {
		fullName: '',
		email: '',
		password: '',
	},
	title: 'Register Page',
	desc: 'Ini adalah deskripsi untuk register page',
};
const registerReducer = (state = initialStateRegister, action) => {
	if (action.type === 'SET_TITLE') {
		return {
			...state,
			title: 'Register ganti title',
		};
	}
	if (action.type === 'SET_FORM') {
		return {
			...state,
			form: {
				...state.form,
				[action.inputType]: action.inputValue,
			},
		};
	}
	return state;
};

/**
 * setting login reducer
 */
const initialStateLogin = {
	info: 'tolong masukan password anda',
	isLogin: false,
};
const loginReducer = (state = initialStateLogin, action) => {
	return state;
};

// const reducer = (state = initialState, action) => {
// 	return state;
// };
const reducer = combineReducers({
	registerReducer,
	loginReducer,
});

export default reducer;
