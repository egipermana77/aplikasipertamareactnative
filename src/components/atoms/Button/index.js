import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

const Button = ({title, onPress}) => {
	return (
		<TouchableOpacity
			style={{
				backgroundColor: '#9708ee',
				borderRadius: 25,
				paddingVertical: 11,
			}}
			onPress={onPress}
		>
			<Text
				style={{
					fontSize: 12,
					fontWeight: 'bold',
					color: 'white',
					textTransform: 'uppercase',
					textAlign: 'center',
				}}
			>
				{title}
			</Text>
		</TouchableOpacity>
	);
};

export default Button;
