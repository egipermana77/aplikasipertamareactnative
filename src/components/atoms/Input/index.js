import React from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {colors} from './../../../utils';

const Input = ({placeholder, ...res}) => {
	return (
		<TextInput
			style={styles.input}
			placeholder={placeholder}
			placeholderTextColor={colors.default}
			{...res}
		/>
	);
};

const styles = StyleSheet.create({
	input: {
		borderWidth: 1,
		borderColor: colors.default,
		borderRadius: 25,
		paddingVertical: 12,
		paddingHorizontal: 18,
		fontSize: 14,
		color: colors.default,
	},
});

export default Input;
