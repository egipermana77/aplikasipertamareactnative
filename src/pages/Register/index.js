import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {Input, Button} from './../../components';
import {colors} from './../../utils';
import {ArrowIcon, Illustration} from './../../../images/';
import {useSelector, useDispatch} from 'react-redux';
import {setForm} from './../../../redux/';

const Register = () => {
	//set global state redux
	const {form} = useSelector(state => state.registerReducer);
	const dispatch = useDispatch();

	// const [form, setForm] = useState({
	// 	fullName: '',
	// 	email: '',
	// 	password: '',
	// });

	useEffect(() => {
		// console.log('redux globalState: ', registerReducer);
	});

	const sendData = () => {
		console.log('data yang dikirim : ', form);
	};

	const onInputChange = (value, inputType) => {
		// setForm({
		// 	...form,
		// 	[input]: value,
		// });
		dispatch(setForm(inputType, value));
	};

	return (
		<View style={styles.wrapper.pages}>
			<ScrollView showsHorizontalScrollIndicator={false}>
				<ArrowIcon height={25} width={25} style={{color: colors.default}} />
				<Illustration height={106} width={115} />
				<Text style={styles.text.desc}>
					Mohon mengisi beberapa data untuk proses
				</Text>
				<View style={styles.space(64)} />
				<Input
					placeholder="Nama Lengkap"
					value={form.fullName}
					onChangeText={valueinput => onInputChange(valueinput, 'fullName')}
				/>
				<View style={styles.space(33)} />
				<Input
					placeholder="Email"
					value={form.email}
					onChangeText={valueinput => onInputChange(valueinput, 'email')}
				/>
				<View style={styles.space(33)} />
				<Input
					placeholder="Password"
					value={form.password}
					onChangeText={valueinput => onInputChange(valueinput, 'password')}
					secureTextEntry={true}
				/>
				<View style={styles.space(83)} />
				<Button title="Daftar" onPress={sendData} />
			</ScrollView>
		</View>
	);
};

const styles = {
	wrapper: {
		pages: {
			padding: 20,
		},
	},
	illustration: {
		width: 106,
		height: 115,
		marginTop: 8,
	},
	text: {
		desc: {
			fontSize: 14,
			fontWeight: 'bold',
			color: colors.default,
			marginTop: 16,
			maxWidth: 200,
		},
	},
	space: value => {
		return {
			height: value,
		};
	},
};

export default Register;
