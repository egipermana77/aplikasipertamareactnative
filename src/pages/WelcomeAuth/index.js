import React from 'react';
import {View, Text, Image} from 'react-native';
import ActionButton from './ActionButton';
import {colors} from './../../utils/';
import {wall} from './../../../images/';

const WelcomeAuth = ({navigation}) => {
	const handleGoTo = screen => {
		navigation.navigate(screen);
	};
	return (
		<View style={styles.wrapper.pages}>
			<Image source={wall} style={styles.wrapper.illustration} />
			<Text style={styles.text.welcome}>Selamat Datang Violet</Text>
			<ActionButton
				desc="Silahkan masuk jika anda sudah memiliki akun"
				title="Masuk"
				onPress={() => handleGoTo('Login')}
			/>
			<ActionButton
				desc="atau, silahkan daftar jiak anda belum memiliki akun"
				title="Daftar"
				onPress={() => handleGoTo('Register')}
			/>
		</View>
	);
};

/**
 * dalam onPress tambahkan ()=> atau arrow function agar tidak langsung action
 */

const styles = {
	wrapper: {
		pages: {
			flex: 1,
			alignItems: 'center',
			justifyContent: 'center',
			backgroundColor: 'white',
		},
		illustration: {
			width: 219,
			height: 117,
			marginBottom: 10,
		},
	},
	text: {
		welcome: {
			fontSize: 18,
			fontWeight: 'bold',
			color: colors.default,
			marginBottom: 76,
		},
	},
};

export default WelcomeAuth;
