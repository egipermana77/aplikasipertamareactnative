export const colors = {
	default: '#9708ee',
	disable: '#a5a5a5',
	dark: '#1d233e',
	text: {
		default: '#7e7e7e',
	},
};
